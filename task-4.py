from flask import Flask, request
app = Flask(__name__)

@app.route("/")
def root():
  return "The default, 'root' route"

@app.route("/account/", methods = ['GET', 'POST'])
def account():
  if request.method == 'POST':
    print request.form
    name = request.form['name']
    return "Hello %s" % name
  else:
    page = '''
    <html>
      <body>
        <form action="" method="post" name="form">
          <label for="name">Name:</label>
          <input type="text" name="name" id+"name"/>
          <input type="submit" name="submit" id="submit"/>
        </form>
      </body>
    </html>'''

    return page

@app.route("/hello/<name>")
def hello(name):
  return "Hello %s" % name

@app.route("/add/<int:first>/<int:second>")
def add(first, second):
  return str(first + second)

@app.route("/hello2/")
def hello2():
    name=request.args.get('name', '')

    if name == '':
      return "no param supplied"
    else:
      return "Hello %s" % name

@app.route("/hello3/", methods = ['POST', 'GET'])
def hello3():
  if request.method == 'POST':
    f = request.files['datafile']
    f.save('static/uploads/file.png')
    return "File Uploaded"

  else:
    page = '''
    <html><body>
      <form action="" method="post" name="form" enctype="multipart/form-data">
        <input type="file" name="datafile"/>
        <input type="submit" name"submit" id="submit-2"/>
      </form>
    </html>'''

    return page, 200

if __name__ == "__main__":
  app.run(host='0.0.0.0', debug = True)
